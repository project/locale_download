# Drupal Locale Download

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

The purpose of this module is to provide a controllable way to download .po
files from the Drupal remote server to local without changing configs and
manually running individual check and update commands.

### Drush Command

A Drush command is provided to use this module:  

```
drush locale:download
```

Executing this command will first check all translations for all projects and
then downloads missing translation files to your translations path.

Invidivual langcodes or projects may be specified by using the `--projects`
or `--langcodes` flags respectively (flag arguments are a comma separated list).

**Example (to download translations for `drupal` and `admin_toolbar`
project only and `de` language)**

```
drush locale:download --projects drupal,admin_toolbar --languages de
```

>Sometimes the translation status cache and the file structure might be
>out of sync.
>Re-running the drush command with the `--force` flag deletes the cache and
>downloads all missing files.

## Requirements

The core module requires Drupal 10.3 or 11 and Drush 12.0 or higher.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module does not require any configuration.

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
