<?php

declare(strict_types=1);

namespace Drupal\locale_download\Drush\Commands;

use Drupal\locale_download\Service\LocaleDownloadService;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drush\Attributes as CLI;

/**
 * Drush commands for locale_download module.
 */
class LocaleDownloadCommands extends DrushCommands {

  use AutowireTrait;

  public function __construct(
    protected LocaleDownloadService $downloader,
  ) {}

  /**
   * Downloads all translation files from remote to local.
   */
  #[CLI\Command(name: 'locale:download')]
  #[CLI\Option(name: 'projects', description: 'Comma separated list of projects (modules) to download. Defaults to all projects.')]
  #[CLI\Option(name: 'langcodes', description: 'Comma separated list of language codes to download. Defaults to all locales.')]
  #[CLI\Option(name: 'force', description: 'Clears static cache before downloading.')]
  public function download(
    $options = [
      'projects' => NULL,
      'langcodes' => NULL,
      'force' => FALSE,
    ],
  ): void {
    if ($projects = $options['projects']) {
      $projects = is_string($projects) ? explode(',', $projects) : NULL;
    }

    if ($langcodes = $options['langcodes']) {
      $langcodes = is_string($langcodes) ? explode(',', $langcodes) : NULL;
    }

    $this->downloader->download(
      $projects,
      $langcodes,
      $options['force'],
    );
  }

}
