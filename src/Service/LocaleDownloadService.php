<?php

declare(strict_types=1);

namespace Drupal\locale_download\Service;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Service to download translation files.
 */
class LocaleDownloadService {

  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
  ) {}

  /**
   * Downloads all translation files from remote to local.
   */
  public function download($specificProjects = [], $specificLangcodes = [], $force = FALSE): void {
    $this->moduleHandler->loadInclude('locale', 'inc', 'locale.translation');
    $this->moduleHandler->loadInclude('locale', 'inc', 'locale.compare');
    $this->moduleHandler->loadInclude('locale', 'inc', 'locale.batch');
    $this->moduleHandler->loadInclude('locale', 'inc', 'locale.fetch');

    $projects = array_keys(locale_translation_get_projects());
    $langcodes = array_keys(locale_translatable_language_list());

    if ($specificProjects) {
      $projects = array_intersect($projects, $specificProjects);
    }

    if ($specificLangcodes) {
      $langcodes = array_intersect($langcodes, $specificLangcodes);
    }

    $projects = array_unique($projects);
    $langcodes = array_unique($langcodes);

    // Clear status cache if forced.
    if ($force) {
      locale_translation_status_delete_projects($projects);
    }

    $this->setTranslationStatusCheckBatch($projects, $langcodes);
    $this->setRemoteTranslationDownloadBatch($projects, $langcodes);

    drush_backend_batch_process();
  }

  /**
   * Checks if remote translations updates are available.
   *
   * @param array $projects
   *   List of projects to check.
   * @param array $langcodes
   *   List of languages to check.
   */
  private function setTranslationStatusCheckBatch(array $projects, array $langcodes) {
    // Check translation status of all translatable project in all languages.
    // First we clear the cached list of projects. Although not strictly
    // necessary, this is helpful in case the project list is out of sync.
    locale_translation_flush_projects();

    // Set the translation import options. This determines if existing
    // translations will be overwritten by imported strings.
    $translationOptions = _locale_translation_default_update_options();
    $translationOptions['use_remote'] = TRUE;

    $batch_builder = (new BatchBuilder())
      ->setFile(\Drupal::service('extension.list.module')->getPath('locale') . '/locale.batch.inc')
      ->setTitle(new TranslatableMarkup('Checking translations'))
      ->setErrorMessage(new TranslatableMarkup('Error checking translation updates.'))
      ->setFinishCallback('locale_translation_batch_status_finished');

    foreach ($projects as $project) {
      foreach ($langcodes as $langcode) {
        $batch_builder->addOperation('locale_translation_batch_status_check', [
          $project,
          $langcode,
          $translationOptions,
        ]);
      }
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * Downloads remote translations.
   *
   * @param array $projects
   *   List of projects to download.
   * @param array $langcodes
   *   List of languages to download.
   */
  private function setRemoteTranslationDownloadBatch(array $projects, array $langcodes) {
    // Set the translation import options. This determines if existing
    // translations will be overwritten by imported strings.
    $translationOptions = _locale_translation_default_update_options();
    $translationOptions['use_remote'] = TRUE;

    $batch_builder = (new BatchBuilder())
      ->setTitle(new TranslatableMarkup('Updating translations.'))
      ->setErrorMessage(new TranslatableMarkup('Error importing translation files'))
      ->setFile(\Drupal::service('extension.list.module')->getPath('locale') . '/locale.batch.inc')
      ->setFinishCallback('locale_translation_batch_fetch_finished');

    foreach ($projects as $project) {
      foreach ($langcodes as $langcode) {
        $batch_builder->addOperation('locale_translation_batch_fetch_download', [
          $project,
          $langcode,
        ]);
      }
    }

    batch_set($batch_builder->toArray());
  }

}
