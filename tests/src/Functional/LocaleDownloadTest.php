<?php

declare(strict_types=1);

namespace Drupal\Tests\locale_download\Functional;

use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\locale\Functional\LocaleUpdateBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Test the locale_download module.
 */
class LocaleDownloadTest extends LocaleUpdateBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'locale',
    'locale_test',
    'locale_download',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::moduleHandler()->loadInclude('locale', 'inc', 'locale.batch');
    ConfigurableLanguage::createFromLangcode('de')->save();

    \Drupal::state()->set('locale.test_projects_alter', TRUE);
    \Drupal::state()->set('locale.remove_core_project', TRUE);

    // Setup the environment.
    $config = $this->config('locale.settings');
    $public_path = PublicStream::basePath();
    $this->setTranslationsDirectory($public_path . '/local');
    $config
      ->set('translation.default_filename', '%project-%version.%language._po')
      ->set('translation.use_source', LOCALE_TRANSLATION_USE_SOURCE_LOCAL)
      ->save();
  }

  /**
   * Test the `locale:download` drush command.
   *
   * This test just makes sure that the command can run.
   * It does not test the actual download functionality.
   */
  public function testLocaleDownload(): void {
    $this->drush('locale:download');
  }

}
